# maze_mobile_robot
The idea of these project is to make a robot which is capable to avoid obstacles and move through of the maze.

# Creating the Project
## 1. Creating the catkin directory
Fisrt, I go to directory src and created a package called mbot_description, mbot_gazebo and mbot_navigation using the following command : `mkdir -p ~/catkin_ws/src` These package contains the main's files project
## 2. Creating the package
You need to open the terminal to install the ROS `catkin_create_pkg mbot rospy`
## 3. Creating the mobile robot model 
In the m`bot_description packages`, use following command:
```
mkdir urdf
mkdir meshes
mkdir launch
mkdir config
Creating the base mobile robot model in the file urdf
touch mbot.urdf 
```
## 4. Creating the world
First use the command "gazebo" to open the gazebo and creat the world to save as the name.world in the `mbot_gazebo/world`
## 5. Creating the launch file use following command
I created manually the .launch, to inculde the world and python codes supposed to be run directly after launch the project. the .launch is an XML format `catkin_creat_pkg name.launch`
## 6. Use the SLAM function package ,install cartographer
sudo apt-get install ros-melodic-cartographer -*
# How to run the project
## 1. Cloning the ROS 
First you have to clone the repository from gitlab, or download the .zip file and unzip it on the home directory.
## 2. Create the folders for workspace
In home directory create workspace and source folders using the following command 
```
mkdir -p ~/catkin_ws/src
```
## 3. Creating the package
Now package using the following commands.
```
cd ~/catkin_ws/src/
```
## 4. Moving the folders from repository
After creating the package now let's move the launch folder,world and scripts folders to our package directory
```
/home/<user>/catkin_ws/src/mbot_description
/home/<user>/catkin_ws/src/mbot_gazebo
/home/<user>/catkin_ws/src/mbot_navigation
```
## 5. Building our workspace
Now the workspace is ready to be built, it can be built using the following command.
```
catkin_make
```    
## 6. Setup the workspace
Then you have to setup the workspace using the following command 

```
sourch devel/setup.h
```
## 7. Launching
After seting up the workspace all you have to do is to launch the package using the following command. 

```
roslaunch mbot_gazebo view_mbot_with_lasser_gazebo.launch
```
open a new terminal 

```
roslaunch mbot_navigation exploring_slam_demo.launch
```

